<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('Halaman.Form');
    }

    public function kirimForm(Request $req){
        $fname = $req['fName'];
        $lname = $req['lName'];
        return view('Halaman.Home', compact('fname','lname'));
    }
}
