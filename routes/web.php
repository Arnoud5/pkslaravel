<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/Form','AuthController@form');

Route::post('/Home','AuthController@kirimForm');

Route::get('/Data-table', function(){
    return view('Table.Data-table');
});


Route::group(['middleware' => ['auth']], function () {
    // crud cast
    // create
    Route::get('/cast/create','CastController@create'); //mengarah ke form tambah cast 
    Route::post('/cast','CastController@store'); //menyimpan data form ke database table cast
    // read
    Route::get('/cast','CastController@index'); //menampilkan semua data dari database table cast ke blade
    Route::get('/cast/{cast_id}','CastController@show'); //menampilkan detail data per id
    // update
    Route::get('/cast/{cast_id}/edit','CastController@edit'); //mengarah ke form mengedit data tabel cast by id
    Route::put('/cast/{cast_id}','CastController@update'); //buat update data di database
    // delete
    Route::delete('/cast/{cast_id}','CastController@destroy'); //route delete data berdsarkan id
});


// autentikasi
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
