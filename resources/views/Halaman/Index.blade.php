@extends('Layout.Master')

@section('judul')
Halaman Index
@endsection

@section('content')
<h1>Media Online</h1>
    <h2>Social Media Developer</h2>
    <p>Belajar dan berbagi agar kehidupan menjadi lebih baik</p>
    <h2>Benefit join di media social</h2>
    <ul>
        <li>Mendapatkan motivsi dari sesama developer</li>
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h2>Cara bergabung ke Media Online</h2>
    <ul>
        <li>Mengunjungi website ini.</li>
        <li>Mendaftarkan di <a href="/Form">Form Sign Up</a>.</li>
        <li>Selesai.</li>
    </ul>    
@endsection