@extends('Layout.Master')

@section('judul')
Halaman Form
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    <h2>Sign up Form</h2>
    <form action="/Home" method="post">
        @csrf
        <label>First Name:</label> <br> <br>
        <input type="text" name="fName"> <br> <br>

        <label>Last Name:</label> <br> <br>
        <input type="text" name="lName"> <br> <br>

        <label">gender:</label> <br> <br>
        <input type="radio" name="gender" value="Male">Male <br>
        <input type="radio" name="gender" value="Female">Female <br> <br>

        <label>Nationality</label> <br> <br>
        <select name="nationality" id=""> <br>
            <option value="id">Indonesia</option> <br>
            <option value="sg">Singapore</option> <br>
            <option value="my">Malaysia</option> <br>
        </select> <br> <br>

        <label>Language Spoken</label> <br> <br>
        <input type="checkbox" name="langSpoken" value="id">Bahasa Indonesia <br>
        <input type="checkbox" name="langSpoken" value="eng">English <br>
        <input type="checkbox" name="langSpoken" value="oth">Other <br> <br>

        <label>Bio</label> <br> <br>
        <textarea name="bioMessage" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit"> <br>
    </form>
@endsection
