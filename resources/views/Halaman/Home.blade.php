@extends('Layout.Master')

@section('judul')
Halaman Index
@endsection

@section('content')
    <h1>Selamat Datang! {{$fname}} {{$lname}}</h1>
    <h2>Terimakasih telah bergabung di website kami. Media Belajar bersama!
    </h2>
@endsection