@extends('Layout.Master')

@section('judul')
Halaman tambah Cast
@endsection

@section('content')

<form method="POST" action="/cast"> 
    @csrf
    <div class="form-group">
        <label>Nama :</label>
        <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Umur :</label>
        <input type="Text" name="umur" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Bio :</label>
        <input type="Text" name="bio" class="form-control">
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection