@extends('Layout.Master')

@section('judul')
Edit CAST's 
@endsection

@section('content')

<form method="POST" action="/cast/{{$cast->id}}"> 
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama :</label>
        <input type="text" name="nama" class="form-control" value="{{$cast->nama}}">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Umur :</label>
        <input type="Text" name="umur" class="form-control" value="{{$cast->umur}}">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Bio :</label>
        <input type="Text" name="bio" class="form-control" value="{{$cast->bio}}">
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection