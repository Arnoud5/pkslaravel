@extends('Layout.Master')

@section('judul')
CAST's
@endsection

@section('content')

<a href="/cast/create" class="btn btn-dark mb-3">Tambah Cast</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key +1 }}</td>
                <td>{{$item -> nama}}</td>
                <td>{{$item -> umur}}</td>
                <td>{{$item -> bio}}</td>
                <td class="d-flex">
                    <a  class="btn btn-dark p-0 px-2" href="/cast/{{$item->id}}">Detail</a>
                    <a  class="btn btn-dark p-0 px-2 mx-2" href="/cast/{{$item->id}}/edit">Edit</a>
                    <form action="/cast/{{$item->id}}" method="POST" class="inline-flex">
                        @csrf
                        @method('delete')
                        <input type="submit" value="delete" class="btn btn-dark p-0 px-2">
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data tidak ada</h1>
        @endforelse
    </tbody>
</table>
@endsection

